<?php

namespace jos\jd;//改成你的namespace
use jos\jd\JdClient;
use jos\jd\request\KplOpenUnionSearchByelitedRequest;
class example
{
    public function test()
    {
        //测试demo
        $c = new JdClient();
        $c->appKey = "46a496130f5ee82a47d8b6433997c7d9";
        $c->appSecret = "08ecdd83929b4d09be4ceda5cf4ec2d9";
        $c->accessToken = "36eef3572be84f6abb3cf2ff84794eafwyzn";
        $c->version = "1.0";//sdk版本必填 因为默认只调用2.0版本的接口,此处重点注意!!!
        $req = new KplOpenUnionSearchByelitedRequest();
        $unionSearchParam= [
			'eliteId'=>1,//	频道id：1-好券商品,2-超级大卖场,10-9.9专区,22-热销爆品,23-为你推荐,24-数码家电,25-超市,26-母婴玩具,27-家具日用,28-美妆穿搭,29-医药保健,30-图书文具,31-今日必推,32-品牌好货,33-秒杀商品,34-拼购商品,109-新品首发,110-自营,125-首购商品,129-高佣榜单,130-视频商品
			'pageIndex'=>1,
			'pageSize'=>20,//每页数量，默认20，上限50
			'sortName'=>'commissionShare',//排序字段(price：单价, commissionShare：佣金比例, commission：佣金， inOrderCount30DaysSku：sku维度30天引单量，comments：评论数，goodComments：好评数)
			'sortType'=>'desc',//	asc,desc升降序,默认降序
        ];
        $req->setUnionSearchParam($unionSearchParam);
        $resp = $c->execute($req, $c->accessToken);
        print(json_encode($resp));
    }
   
}